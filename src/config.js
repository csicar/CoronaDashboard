import numeral from 'numeral'

const numberFormat = new Intl.NumberFormat()

export default {
  responsive: true,
  maintainAspectRatio: false,
  height: 200,
  tooltips: {
    mode: "index",
    intersect: false,
  },
  hover: {
    mode: "nearest",
    intersect: true,
  },
  legend: {
    labels: {

      fontColor: 'white',
    }
  },
  scales: {
    xAxes: [
      {
        display: true,
        type: "time",
        time: {
          parser: "YYYY-MM-DD",
          // round: 'day'
          tooltipFormat: "YYYY-MM-DD",
          unit: "month",
        },
        scaleLabel: {
          display: false,
          labelString: "Day",
        },
        gridLines: {
          color: "rgba(171,171,171,0.3)",
        },
      },
    ],
    yAxes: [
      {
        display: true,
        scaleLabel: {
          display: true,
          labelString: "Cases",
        },
        ticks: {
          callback(value) {
            return numberFormat.format(value);
          }
        },
        gridLines: {
          color: "rgba(171,171,171,0.3)",
        },
      },
    ],
  },
}