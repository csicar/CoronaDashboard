
import Vue from 'vue';
import App from './App.vue';
import 'materialize-css/dist/css/materialize.min.css'

new Vue({ render: createElement => createElement(App) }).$mount('#app');