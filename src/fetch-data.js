const parse = require('csv-parse')
const fetch = require('node-fetch')
const fs = require('fs')

async function fetchInfectionData() {
  const req = await fetch("https://raw.githubusercontent.com/RamiKrispin/coronavirus/master/csv/coronavirus.csv")
  fs.writeFileSync('./data.json', await req.text());
}

async function fetchVaccinationData() {
  const req = await fetch("https://github.com/govex/COVID-19/raw/master/data_tables/vaccine_data/global_data/time_series_covid19_vaccine_global.csv")
  fs.writeFileSync('./vaccination.json', await req.text());
}

async function appendVaccinationData(output)  {
  const parser = parse({
    delimiter: ','
  })

  parser.on('readable', () => {
    let record
    parser.read();
    while (record = parser.read()) {
      const [country,date,dosesAdmin,peoplePartiallyVaccinated,peopleFullyVaccinated,report,uid] = record
      if(output[country]) {
        output[country] = output[country] || {}
        output[country][date] = output[country][date] || {};
        output[country][date]["dosesAdmin"] = dosesAdmin
        output[country][date]["peoplePartiallyVaccinated"] = peoplePartiallyVaccinated
        output[country][date]["peopleFullyVaccinated"] = peopleFullyVaccinated
      } else {
        console.log(`ignoring ${country}`)
      }
    }
  })
  parser.on('error', (err) => {
    console.error(err.message)
  })
  parser.on('end', () => {
    parser.end()
    // console.table(output)

    fs.writeFileSync('./compiled.json', JSON.stringify({data: output, last_updated: new Date()}))

  })

  fs.createReadStream('./vaccination.json').pipe(parser);


}

async function main() {
  await fetchInfectionData();
  await fetchVaccinationData();
  const parser = parse({
    delimiter: ','
  })
  const output = {}
  parser.on('readable', () => {
    let record
    parser.read();
    while (record = parser.read()) {
      const [date, province, country, lat, long, type, cases] = record
      output[country] = output[country] || {}
      output[country][date] = output[country][date] || {};
      output[country][date][type] = cases
    }
  })
  parser.on('error', (err) => {
    console.error(err.message)
  })
  parser.on('end', () => {
    parser.end()
    // console.table(output)

    appendVaccinationData(output);

  })

  fs.createReadStream('./data.json').pipe(parser);
}

main();
